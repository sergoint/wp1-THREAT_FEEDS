from pymisp import MISPEvent
from pymisp import PyMISP, ExpandedPyMISP
from pymisp import MISPAttribute
from io import BytesIO
import urllib3
urllib3.disable_warnings()

#misp access
misp_key = '...' # get from https://<mispserver>/events/automation
misp_url = 'https://mymispserver/'
misp_verifycert = False # or False if you are using self-signed cert for testing
misp = ExpandedPyMISP(misp_url, misp_key, misp_verifycert)


with open('data.txt', 'a') as out:


# Search attributes (specified in controller) where the attribute type is 'ip-src'
    attributes = misp.search(controller='attributes', type_attribute='ip-src', pythonify=True)


# Collect all event_id matching the searched attribute
    event_ids = set()
    for attr in attributes:
        event_ids.add(attr.event_id)

# Fetch & print src ips related to an event
    for event_id in event_ids:
        event = misp.get_event(event_id)
        #print(event.info)
        out.write(event.info + '\n')
        event_attrs = misp.search(controller='attributes', type_attribute='ip-src', eventid=event_id, pythonify=True)
        for attr in event_attrs:
            out.write(attr.value + '\n')
            #print(attr.value)



