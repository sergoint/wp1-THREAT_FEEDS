# THREAT_FEEDS

Tools for collecting daily snapshots of publicly available TI feeds and analysis
of how dynamic and timely they are.

# How
Running (requires python 2.7)
~~~~
poll.py 
~~~~
pulls feeds listed in format.json to ./data folder and
~~~~
format.py 
~~~~
formats them to standardized json format to ./data_json folder. 

These can be automated by cron. For example adding the following to crontab
~~~~
0 6 * * * /path/to/poll.py
10 6 * * * /path/to/format.py
~~~~
runs scripts daily at 6am and 6:10 am respectively. 

note: Scripts assume that snapshots are made at most once a day.

#

Running
~~~~
analyze_feeds.py
~~~~
creates
~~~~
dynamicity.txt
timeliness.txt
~~~~


# FEATURE_EXTRACTOR

See the [feature_extractor repository](https://gitlab.com/cincan/feature_extractor)
