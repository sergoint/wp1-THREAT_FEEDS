#!/usr/bin/env python2
from six.moves import urllib
from io import open
import os
import json
import datetime

dateTimeObj = datetime.datetime.now()
timestampStr = dateTimeObj.strftime("%Y-%m-%d--%H-%M-%S")

if not os.path.exists('data'):
    os.makedirs('data')

with open("format.json", encoding='utf-8') as file:
    data = json.load(file)
for d in data:
    url = d["url"]
    source =d["source"]
    source = source.replace(" ", "_")
    urllib.request.urlretrieve(url, './data/'+source+"--"+timestampStr+'.txt')
