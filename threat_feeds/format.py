#!/usr/bin/env python2
# -*- coding: utf-8 -*-
import os, sys, json, re, time, socket
from io import open


if sys.version_info >= (3, 0):
    unicode = str


data_path = "./data/"
output_path = "./data_json/"
#current_file=""

def extract_banner(file):
#    with open(data_path + filename, encoding='utf-8') as file:
    banner = ""
    file.seek(0)
    for line in file:
        if line.startswith("#"):
            banner+=str(line)
    return banner

def extract_data(file, source):
    a = dict(data= [])
    
    if "removefirst" in source:
        firstline = True
    else:
        firstline = False
    f = source["format"]
    if "separator" in source:
        s = source["separator"]
    else:
        s = None
    if "comment" in source:
        c = source["comment"]
    else:
        c="\n"
    while firstline:
        line = file.readline()
        if not line.startswith(c) and not re.match(r'^\s*$', line):  # line is empty (has only the following: \t\n\r and whitespace)
            firstline = False

    if s == '\t':
        for line in file:
            if line.startswith(c) or re.match(r'^\s*$', line):  # line is empty (has only the following: \t\n\r and whitespace)
                continue  
            n = 0
            m = dict()
            x = re.split(r'\t+', line)
            for b in source["format"]:       
                if b == "null":
                    n=n+1
                    continue   
                if len(x)<n+1:
                    continue
                m[b] = x[n].rstrip("\r\n\", ").lstrip("\" #")            
                n+=1
            a["data"].append(m)
    elif s:
        for line in file:
            if line.startswith(c) or re.match(r'^\s*$', line):  # line is empty (has only the following: \t\n\r and whitespace)
                continue
            
            n = 0
            m = dict()            
            x = line.split(s, len(source["format"])-1)
            for b in source["format"]:
                if b == "null":
                    n=n+1
                    continue
                if len(x)<n+1:
                    continue
                m[b] = x[n].rstrip("\r\n\", ").lstrip("\" #")
                n=n+1
            a["data"].append(m)
    else:
        b = source["format"][0]
        for line in file:
            if line.startswith(c) or re.match(r'^\s*$', line):  # line is empty (has only the following: \t\n\r and whitespace)
                continue
            n = 0
            m = dict()
            m[b] = line.rstrip("\r\n\", ").lstrip("\" #")
            a["data"].append(m)
    return a

#consider replacing inet_aton which ip_address. inet_aton accepts strings like '4', '4.4' padding with zeros to get valid IP
def is_ipv4(ipv4):
    try:
        ##ipaddress.ip_address(ipv4)
        socket.inet_aton(ipv4)
        return True
    except socket.error:
        ##except ValueError:
#        with open(errors_path + current_file ,'a', encoding='utf-8') as out:      
#            out.write(ipv4 + '\n')
        return False

def sort(data, d):
#    global current_file
    #if not os.path.exists(errors_path):
        #os.makedirs(errors_path)
    if "domain" in d["format"]:
        data["data"] = sorted(data["data"], key=lambda item: item["domain"])
    if "ip-block" in d["format"]:
        data["data"] = sorted(data["data"], key=lambda item: item["ip-block"])
    if "url" in d["format"]:
        data["data"] = sorted(data["data"], key=lambda item: item["url"])
    if "ip" in d["format"]:
        data["data"][:] = [tup for tup in data["data"] if is_ipv4(tup["ip"])] #remove entries which are not valid ipv4
        data["data"] = sorted(data["data"], key=lambda item: socket.inet_aton(item["ip"]))
    return data 


def open_file(new = False): #file name feed_name--%Y-%m-%d--%H-%M-%S.txt feed_name 
    if not os.path.exists(output_path):
        os.makedirs(output_path)
    with open("format.json", encoding='utf-8') as file:
        tdata = json.load(file)
    for d in tdata:
        if "encoding" in d:
            e = d["encoding"]
        else:
            e = "utf-8"
        feed =d["source"].replace(" ", "_")    
        files = os.listdir(output_path)
        for i in os.listdir(data_path):
            name,_ = i.rsplit('.',1)
            name = name + '.json'

            if new and name in files:
                continue
            if  i.startswith(feed) and i.endswith(".txt"):
                print(i)

                with open(data_path + i, encoding=e) as f:
                    data = extract_data(f, d)
                    data = sort(data, d)
                    if "banner" in d:
                        banner= extract_banner(f)
                        k = dict(banner=banner)
                        data["meta"] = k
                        #print(banner)
                a = json.dumps(data, indent=None, sort_keys=True)
                x = i.rsplit(".", 1)
                with open(output_path + x[0] + '.json', 'w', encoding='utf-8') as outfile:  
                    outfile.write(unicode(a))


# {"data": [ {
#                "ip": "",
#                 "created": ""
#               },
#             {
#                "ip": "",
#                 "created": ""
#               },    
#           ],
#    "meta" : {
#               "banner": ""
#               "created": ""
#}}
#
#
#
#



def main():
    #start_time = time.time()

    if  len(sys.argv) == 1:
        print "Updating new files. To format all files run with parameter --all"
        open_file(True)
    elif sys.argv[1] == "--all":
        open_file()
    else:
        print "Invalid input parameter"

    #print("--- %s seconds ---" % (time.time() - start_time))
if __name__ == '__main__':
    main()
